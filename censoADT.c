#include "censoADT.h"
#include <stdlib.h>
#include "dataADT.h"
#include <stdio.h>
#include <string.h>
#define PUNTO 2
#define LONG_IND PUNTO+3

typedef struct  {
    unsigned int ocupados;
    unsigned int desocupados;
    unsigned int total_personas;
} thabitantes;

typedef struct dep{
    char * nombre_departamento;
    thabitantes habitantes;
    struct dep * siguiente;
} tdep;

typedef struct provincia{
    char * nombre_provincia;
    thabitantes habitantes;
    tdep * departamento;
    struct provincia * siguiente;
} tprovincia;
typedef struct censoCDT{
    tprovincia * provincias;
    tprovincia * iterProv;
    tdep * iterDep;
    thabitantes habitantes;
} censoCDT;


static void iniHabitantes(thabitantes * habitantes){
    habitantes->ocupados = 0;
    habitantes->desocupados = 0;
    habitantes->total_personas = 0;
}
censoADT newCenso(void){
    censoADT censo= malloc(sizeof(*censo));
    iniHabitantes(&(censo->habitantes));
    censo->provincias=NULL;
    censo->iterProv=NULL;
    censo->iterDep=NULL;
    return censo;
}
void aumentarHabitantes(thabitantes * habitantes, char condicion){
    habitantes->total_personas++;
    switch(condicion){
        case '1':
            habitantes->ocupados++;
            break;
        case '2':
            habitantes->desocupados++;
            break;
    }
}
void liberarDeptos(tdep * depto){
    tdep * aux;
    while(depto != NULL){
        free(depto->nombre_departamento);
        aux = depto->siguiente;
        free(depto);
        depto = aux;
    }
}
void liberarProvs(tprovincia * prov){
    tprovincia * aux;
    while(prov != NULL){
        liberarDeptos(prov->departamento);
        free(prov->nombre_provincia);
        aux = prov->siguiente;
        free(prov);
        prov = aux;
    }
}
void liberarCenso(censoADT censo){
    liberarProvs(censo->provincias);
    free(censo);
}




tdep * agregarDeptoRec(tdep * depto, char condicion, char * nombre, tprovincia * prov, censoADT censo){
    int c;
    if (depto == NULL || (c = strcmp( depto->nombre_departamento,nombre)) > 0)
    {
        tdep * aux = malloc(sizeof(*aux));
        if (aux == NULL)
        {
            return depto;
        }
        aux->siguiente = depto;
        iniHabitantes(&aux->habitantes);
        aux->nombre_departamento = malloc(strlen(nombre)+1);
        if (aux->nombre_departamento == NULL)
        {
            free(aux);
            return depto;
        }
        strcpy(aux->nombre_departamento, nombre);
        aumentarHabitantes(&aux->habitantes, condicion);
        aumentarHabitantes(&prov->habitantes, condicion);
        aumentarHabitantes(&censo->habitantes, condicion);
        return aux;
    }
    if (c == 0)
    {
        aumentarHabitantes(&depto->habitantes, condicion);
        aumentarHabitantes(&prov->habitantes, condicion);
        aumentarHabitantes(&censo->habitantes, condicion);
        return depto;
    }
    depto->siguiente = agregarDeptoRec(depto->siguiente, condicion, nombre,prov, censo);
    return depto;
}

int agregarDepto(tdep ** depto, char * ndep, char condicion,  tprovincia * prov, censoADT censo){
    unsigned int tam = prov->habitantes.total_personas;
    *depto = agregarDeptoRec(*depto, condicion, ndep, prov, censo);
    return tam != prov->habitantes.total_personas;
}


static provADT addProv(provADT p,char * prov,char * depto,char cond,censoADT censo){
    int c;
    if(p==NULL || (c=strcmp(p->nombre_provincia,prov))>0){
        provADT new=malloc(sizeof(*new));
        if(new==NULL){
            return p;
        }
        unsigned long longitud=strlen(prov)+1;
        new->nombre_provincia=malloc(longitud);
        if(new->nombre_provincia==NULL){
            free(new);
            return p;
        }
        new->departamento=NULL;
        iniHabitantes(&new->habitantes);
        strcpy(new->nombre_provincia, prov);
        new->siguiente=p;
        p=new;
        agregarDepto(&(p->departamento),depto,cond,p,censo);
        return p;
    }
    if(c==0){
        agregarDepto(&(p->departamento),depto,cond,p,censo);
        return p;
    }
    p->siguiente=addProv(p->siguiente, prov,depto,cond,censo);
    return p;
}

int add(char * prov,char * depto,char cond,censoADT c){
    unsigned long poblacionant=c->habitantes.total_personas;
    c->provincias=addProv(c->provincias, prov,depto,cond, c);
    if(poblacionant==c->habitantes.total_personas){
        return 0;
    }
    return 1;
}

int siguienteProv(censoADT censo){
    if(censo->iterProv == NULL){
        return 0;
    }
    censo->iterProv = censo->iterProv->siguiente;
    if(censo->iterProv == NULL){
        return 0;
    }
    censo->iterDep = censo->iterProv->departamento;
    return 1;
}

int siguienteDepto(censoADT censo){
    if(censo->iterDep == NULL){
        return 0;
    }
    censo->iterDep = censo->iterDep->siguiente;
    if(censo->iterDep == NULL){
        return siguienteProv(censo);
    }
    return 1;
}


void iniciar(censoADT censo){
    censo->iterProv = censo->provincias;
    censo->iterDep = censo->iterProv->departamento;
}
static float obtenerIndDesempleo(thabitantes * habitantes){
    return (habitantes->desocupados)/((float) (habitantes->desocupados + habitantes->ocupados));
}
int getPais(censoADT c, float  * indice){
    *indice=obtenerIndDesempleo(&(c->habitantes));
    return c->habitantes.total_personas;
}

int getdepto(censoADT c, char ** provincia, char ** departamento, unsigned int * habitantes, float * indice)
{
    if (habitantes != NULL)
    {
        *habitantes=c->iterDep->habitantes.total_personas;
    }
    if (indice != NULL)
    {
        *indice=obtenerIndDesempleo(&(c->iterDep->habitantes));
    }
    if (provincia != NULL)
    {
        *provincia=malloc(strlen(c->iterProv->nombre_provincia)+1);
        if (*provincia==NULL)
            return 0;
        strcpy(*provincia, c->iterProv->nombre_provincia);
    }
    if (departamento != NULL)
    {
        *departamento=malloc(strlen(c->iterDep->nombre_departamento)+1);
        if (*departamento==NULL)
            return 0;
        strcpy(*departamento, c->iterDep->nombre_departamento);
    }
    return 1;
}

int getProv(censoADT c, unsigned int * habitantes, float * indice, char ** nombre){
    if(indice != NULL){
        *indice = obtenerIndDesempleo(&c->iterProv->habitantes);
    }
    if (habitantes != NULL) {
        *habitantes = c->iterProv->habitantes.total_personas;
    }
    if (nombre != NULL)
    {
        *nombre = malloc(strlen(c->iterProv->nombre_provincia)+1);
        if (*nombre == NULL)
        {
            return 0;
        }
        strcpy(*nombre, c->iterProv->nombre_provincia);
    }
    return 1;
}


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "censoADT.h"
#include "dataADT.h"
#define PUNTO 2
#define LONG_IND PUNTO+3
#define MAX_CARACTERES 80
enum {COND=0,ID,PROV,DEP,ERROR};
#define MAXLENGTH 80
#define NO_SL 50
#define ARCHIVO stdin


typedef struct tdata{
    char condicion;
    char * departamento;
    char * prov;
}tdata;

tdata * newData(void){
    tdata * data=malloc(sizeof(*data));
    data->condicion=0;
    data->departamento=NULL;
    data->prov=NULL;
    return data;
}

void freeData(tdata * data){
    free(data->departamento);
    free(data->prov);
    free(data);
}

/* 
    Esta funcion verifica que lo que lee de archivo cumpla con las condiciones y si es correcto guarda en el TAD
*/

int getValidLine(char * linea,tdata * data,int * ret, FILE * archivo){
    int c,error=0,cont=0,estado=COND,i=0;
    int j=0;
    char caracter[1];
    data->departamento=malloc(80);
    data->prov=malloc(80);
    char * mistake=strchr(linea, '\n');
    if ((data->departamento==NULL)||(data->prov==NULL)) {
        freeData(data);
        return 0;
    }
    if(mistake==NULL){
        *ret=NO_SL;
        freeData(data);
        return 0;
    }
    for (; (c=linea[cont])!=0 && c!='\n' && error==0 && estado<=3 ;cont++ ) {
        if( c>128 || c<0 ){
            error=1;
        }
        if(c==','){
            estado++;
        }else{
        switch (estado) {
            case COND:
                if(c<='3' || c>='0' ){
                    data->condicion=c;
                }else{
                    error=1;
                }
                break;
            case ID:
                break;
            case PROV:
                    caracter[0]=c;
                    strcpy(data->departamento+i,caracter);
                    i++;
                
                break;
            case DEP:
                    caracter[0]=c;
                    strcpy(data->prov+j, caracter);
                    j++;
                
                break;
        }
        }
    }
    
    if( error==1 || estado!=3 ){
        freeData(data);
        *ret=0;
        return 0;
    }
        caracter[0]=0;
        strcpy(data->departamento+i, caracter);
        data->departamento=realloc(data->departamento, i+1);
        *ret=0;
        strcpy(data->prov+j, caracter);
        data->prov=realloc(data->prov, j+1);
        return 1;
    
}



void error (void)
{
    printf("Error\n");
    return ;
}

void exportDepto (censoADT c)
{
    FILE * depto=fopen("Departamento.csv", "wt");
    if (depto==NULL)
        return ;
    char * provincia=NULL;
    char * departamento=NULL;
    unsigned int habitantes;
    float indice;
    do
    {
        if (getdepto(c, &provincia, &departamento, &habitantes, &indice)==0)
            error();
        fwrite(provincia, sizeof(char), strlen(provincia), depto);
        fputc(',' , depto);
        fwrite(departamento, sizeof(char), strlen(departamento), depto);
        fputc(',' , depto);
        fprintf(depto, "%u" ,habitantes);
        fputc(',' , depto);
        fprintf(depto, "%1.*f",PUNTO ,indice);
        fputc('\n', depto);
        free(provincia);
        free(departamento);
    } while(siguienteDepto(c));
    
    fputc(0, depto);
    fclose(depto);
}

int validar (char c)
{ int ok=0;
    if (isdigit(c))
        if ((c-'0')<4 && (c-'0')>=0)
            ok=1;
    return ok;
}


void exportProv(censoADT c){
    FILE * provincia=fopen("Provincia.csv", "wt");
    if (provincia==NULL) {
        printf("No se pudo crear provincia.csv");
        return ;
    }
    char * nombre_provincia=NULL;
    unsigned int habitantes;
    float indice;
    do {
        if(getProv(c,&habitantes,&indice,&nombre_provincia)){
            fwrite(nombre_provincia, sizeof(char), strlen(nombre_provincia), provincia);
            fputc(',' , provincia);
            fprintf(provincia, "%u" ,habitantes);
            fputc(',' , provincia);
            fprintf(provincia, "%1.*f",PUNTO,indice);
            fputc('\n' , provincia);
            free(nombre_provincia);
        }
    }
    while (siguienteProv(c));
    fputc(0, provincia);
    fclose(provincia);
}
void exportPais(censoADT c){
    float indice;
    unsigned int hab=getPais(c,&indice);
    FILE * pais=fopen("Pais.csv", "wt");
    if(pais == NULL){
        return ;
    }
    fprintf(pais, "%u", hab);
    fputc(',',pais);
    fprintf(pais, "%1.*f",PUNTO, indice);
    fputc(0, pais);
    fclose(pais);
}

int main(void) {
    char * linea=malloc(80);
    int carac;
    censoADT c=newCenso();
    while ((fgets (linea, MAX_CARACTERES, ARCHIVO))!=NULL){
        tdata * data=newData();
        int r;
        if(getValidLine(linea, data,&r, ARCHIVO)){
            add(data->prov,data->departamento,data->condicion,c);
            freeData(data);
            
        }
        if(r==NO_SL){
            do {
                carac=fgetc(ARCHIVO);
            } while (carac!='\n' && carac!=EOF);
        }
        
    }
    free(linea);
    exportPais(c);
    iniciar(c);
    exportProv(c);
    iniciar(c);
    exportDepto(c);
    liberarCenso(c);
    return 0;
}

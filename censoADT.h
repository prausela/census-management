//
//  censoADT.h
//  TP ESPECIAL (v2.0)
//
//  Created by Eduardo Lifschitz on 8/12/17.
//  Copyright © 2017 Apple Inc. All rights reserved.
//

#ifndef censoADT_h
#define censoADT_h

#include <stdio.h>
typedef struct censoCDT * censoADT;
typedef struct provincia * provADT;
typedef struct dep * depADT;
int getPais(censoADT c, float  * indice);
int add(char * prov,char * depto,char cond,censoADT c);
int siguienteProv(censoADT censo);
int siguienteDepto(censoADT censo);
int getdepto(censoADT c, char ** provincia, char ** departamento, unsigned int * habitantes, float * indice);
void iniciar(censoADT censo);
censoADT newCenso(void);
void liberarCenso(censoADT censo);
int getProv(censoADT c, unsigned int * habitantes, float * indice, char ** nombre);
#endif /* censoADT_h */

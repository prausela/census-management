Census Management
Upload of project done the second Trimester of 2017. Team members:

* Gaston Lifschitz
* Manuel Luque
* Paula Oseroff

I. File list (including source files)
------------
main.c		Main program
CensusADT.c		an ADT for census management
censusADT.h		includes all the calls for censusADT.c 
				functions
frontEnd.c		Front end functions
FrontEnd.c	includes all the calls for frontEnd.c functions
README 	BinarySearchTree header
fileTest.csv		a File Test of the program


II. RUN
------------
1º	Open your command line terminal and locate in the folder that 	has all the source file and the file test.

2º	Copy this line:
	“gcc -o censo main.c censoADT.c dataADT.c -pedantic -std=c99 -Wall -fsanitize=address”


NOTE: 3 new files have been created in your current folder:
pais.csv				
provincias.csv	
departamento.csv

pais.csv
------------
This file has only one line and stores in the first value the number of people living in the country and in the second value the Unemployment index.


provincia.csv
------------
This file has a line per province/state and it saves the name of the province, the number of people living in that province and the unemployment index.

departamento.csv
------------ 
This file has a line per apartment and it saves the name of the province, the name of the apartment, the number of people living there and the Unemployment index.